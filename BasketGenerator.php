<?php

class BasketGenerator
{
    public $product_probability_list = [
        'iron' => 0.4,
        'copper' => 0.3,
        'gold' => 0.15,
        'platinum' => 0.1,
        'diamond' => 0.05,
    ];
    public $product_volume_list = [
        'iron' => 75,
        'copper' => 75,
        'gold' => 150,
        'platinum' => 500,
        'diamond' => 1000,
    ];

    public $N = 4;
    public $M = 65535;
    public $F = 6;

    public $basket = [];
    public $current_basket_volume = 0;

    function __construct()
    {
    }

    function __construct1($N, $M, $F)
    {
        $this->N = $N;
        $this->M = $M;
        $this->F = $F;
    }

    function generateBasket()
    {
        while ($this->OutOfLoopCondition()) {
            $product = $this->getRandomProduct($this->product_probability_list);
            $this->addProductToBasket($product);
        }
    }

    public function reloadBasket()
    {
        $old_basket = $this->basket;
        do {
            $this->basket = [];
            $this->current_basket_volume = 0;
            $this->generateBasket();
        } while (!boolval(array_diff(array_keys($old_basket), array_keys($this->basket))));
    }

    public function addProductToBasket($product)
    {
        if (count($this->basket) < $this->N || $this->basket[$product] < $this->F && array_key_exists($product, $this->basket)) {
            if ($this->product_volume_list[$product] <= $this->M - $this->current_basket_volume) {
                $this->basket[$product] += 1;
                $this->current_basket_volume += $this->product_volume_list[$product];
            }
        }
    }

    function getRandomProduct(array $set, $length = 10000)
    {
        $coef = 0;
        foreach ($set as $product => $probability) {
            $set[$product] = $coef + $probability * $length;
            $coef = $set[$product];
        }
        $test = mt_rand(1, $length);
        $coef = 1;
        foreach ($set as $num => $probability) {
            if ($test >= $coef && $test <= $probability) {
                return $num;
            }
            $coef = $probability;
        }
        return null;
    }

    function OutOfLoopCondition()
    {
        if (count($this->basket) == 0)
            return true;

        foreach ($this->basket as $product => $count) {
            if (count($this->basket) < $this->N && min(array_values($this->product_volume_list)) <= $this->M - $this->current_basket_volume ||
                $count < $this->F && $this->product_volume_list[$product] <= $this->M - $this->current_basket_volume) {
                return true;
            }
        }
        return false;
    }
}